package com.company;

public class Policy {

    /*
    Class variables
     */

    // long list of variables...
    // could declare private int once but this route is more readable
    private int policyID;
    private String stateCode;
    private String county;
    private float eq_site_limit;
    private float hu_site_limit;
    private float fl_site_limit;
    private float fr_site_limit;
    private float tiv_2011;
    private float tiv_2012;
    private float eq_site_deductible;
    private float hu_site_deductible;
    private float fl_site_deductible;
    private float fr_site_deductible;
    private float point_latitude;
    private float point_longitude;
    private String line;
    private String construction;
    private int point_granularity;

    /*
    Constructor
     */

    // needs improvement
    public Policy(int policyID, String stateCode, String county, float eq_site_limit, float hu_site_limit, float fl_site_limit, float fr_site_limit, float tiv_2011, float tiv_2012, float eq_site_deductible, float hu_site_deductible, float fl_site_deductible, float fr_site_deductible, float point_latitude, float point_longitude, String line, String construction, int point_granularity) {
        this.policyID = policyID;
        this.stateCode = stateCode;
        this.county = county;
        this.eq_site_limit = eq_site_limit;
        this.hu_site_limit = hu_site_limit;
        this.fl_site_limit = fl_site_limit;
        this.fr_site_limit = fr_site_limit;
        this.tiv_2011 = tiv_2011;
        this.tiv_2012 = tiv_2012;
        this.eq_site_deductible = eq_site_deductible;
        this.hu_site_deductible = hu_site_deductible;
        this.fl_site_deductible = fl_site_deductible;
        this.point_latitude = point_latitude;
        this.point_longitude = point_longitude;
        this.line = line;
        this.construction = construction;
        this.point_granularity = point_granularity;
    }

    /*
    Getters and setters
     */

    public float getHu_site_limit() {
        return hu_site_limit;
    }

    public void setHu_site_limit(int hu_site_limit) {
        this.hu_site_limit = hu_site_limit;
    }

    public float getEq_site_limit() {
        return eq_site_limit;
    }

    public void setEq_site_limit(int eq_site_limit) {
        this.eq_site_limit = eq_site_limit;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public int getPolicyID() {
        return policyID;
    }

    public void setPolicyID(int policyID) {
        this.policyID = policyID;
    }

    public float getFl_site_limit() {
        return fl_site_limit;
    }

    public void setFl_site_limit(int fl_site_limit) {
        this.fl_site_limit = fl_site_limit;
    }

    public float getFr_site_limit() {
        return fr_site_limit;
    }

    public void setFr_site_limit(int fr_site_limit) {
        this.fr_site_limit = fr_site_limit;
    }

    public float getTiv_2011() {
        return tiv_2011;
    }

    public void setTiv_2011(int tiv_2011) {
        this.tiv_2011 = tiv_2011;
    }

    public float getTiv_2012() {
        return tiv_2012;
    }

    public void setTiv_2012(int tiv_2012) {
        this.tiv_2012 = tiv_2012;
    }

    public float getEq_site_deductible() {
        return eq_site_deductible;
    }

    public void setEq_site_deductible(int eq_site_deductible) {
        this.eq_site_deductible = eq_site_deductible;
    }

    public float getHu_site_deductible() {
        return hu_site_deductible;
    }

    public void setHu_site_deductible(int hu_site_deductible) {
        this.hu_site_deductible = hu_site_deductible;
    }

    public float getFl_site_deductible() {
        return fl_site_deductible;
    }

    public void setFl_site_deductible(int fl_site_deductible) {
        this.fl_site_deductible = fl_site_deductible;
    }

    public float getPoint_latitude() {
        return point_latitude;
    }

    public void setPoint_latitude(int point_latitude) {
        this.point_latitude = point_latitude;
    }

    public float getPoint_longitude() {
        return point_longitude;
    }

    public void setPoint_longitude(int point_longitude) {
        this.point_longitude = point_longitude;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getConstruction() {
        return construction;
    }

    public void setConstruction(String construction) {
        this.construction = construction;
    }

    public int getPoint_granularity() {
        return point_granularity;
    }

    public void setPoint_granularity(int point_granularity) {
        this.point_granularity = point_granularity;
    }

    public float getFr_site_deductible() {
        return fr_site_deductible;
    }

    public void setFr_site_deductible(float fr_site_deductible) {
        this.fr_site_deductible = fr_site_deductible;
    }


}
