package com.company;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReader {

    private File csvName;
    private String separatorSymbol;
    private List contentsOfFile = new ArrayList<>();
    private List firstRowOfFile = new ArrayList<>();


    /*
     Getters and Setters for instance variables
      */

    public String getSeparatorSymbol() {
        return separatorSymbol;
    }

    public void setSeparatorSymbol(String separatorSymbol) {
        this.separatorSymbol = separatorSymbol;
    }

    public List getContentsOfFile() {
        return contentsOfFile;
    }

    public File getCsvName() {
        return csvName;
    }

    public void setCsvName(File csvName) {
        this.csvName = csvName;
    }

    public void setContentsOfFile(List contentsOfFile) {
        this.contentsOfFile = contentsOfFile;
    }

    List getFirstRowOfFile() {
        return firstRowOfFile;
    }

    void setFirstRowOfFile(List firstRowOfFile) {
        this.firstRowOfFile = firstRowOfFile;
    }

    /*
    Class methods
     */

    // uses stream for readability
    // reads CSV contents into instance variable for later use
    public List readFile() {

        try {
            Files.lines(this.csvName.toPath(), StandardCharsets.UTF_8)
                    .map(line -> Arrays.asList(line.split(this.separatorSymbol)))
                    .forEachOrdered(this.contentsOfFile::add);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.contentsOfFile;

    }

    // stores the first row from the CSV
    List retrieveFirstRow() {

        try {
            Files.lines(this.csvName.toPath(), StandardCharsets.UTF_8)
                    .map(line -> Arrays.asList(line.split(this.separatorSymbol)))
                    .findFirst()
                    .ifPresent(this.firstRowOfFile::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this.firstRowOfFile;
    }

    // creates a list of Policy objects
    public List<Policy> createPolicies(List<Object> csvFile) {

        var result = new ArrayList<Policy>();
        String[] readableInfo = new String[csvFile.size()];
        var disassembledInfo = new ArrayList<String[]>();

        // first - split the object's contents into a readable array
        for (int i = 0; i < csvFile.size(); i++) {
            readableInfo[i] = csvFile.get(i).toString();
        }

        // second - split the array's entries into indie strings and store them too
        // array containing arrays containing a row's info
        for (int x = 1; x < csvFile.size(); x++) {
            disassembledInfo.add(readableInfo[x].split(","));
        }

        // third - map the values of the stored rows to a new Policy object
        for (String[] array : disassembledInfo) {

            for (int s = 0; s < array.length; s++) {

                // remove all commas, brackets, and whitespace
                array[s] = array[s].replaceAll("\\[", "");
                array[s] = array[s].replaceAll("\\]", "");
                array[s] = array[s].replaceAll("\\,", "");
                array[s] = array[s].trim();
            }


            Policy policy = new Policy(
                    Integer.parseInt(array[0]), array[1], array[2], Float.parseFloat(array[3]),
                    Float.parseFloat(array[4]), Float.parseFloat(array[5]), Float.parseFloat(array[6]),
                    Float.parseFloat(array[7]), Float.parseFloat(array[8]), Float.parseFloat(array[9]),
                    Float.parseFloat(array[10]), Float.parseFloat(array[11]), Float.parseFloat(array[12]),
                    Float.parseFloat(array[13]), Float.parseFloat(array[14]), array[15], array[16], Integer.parseInt(array[17])
            );

            // collect the new policies
            result.add(policy);


        }

        // use the new list of Policy objects for db entry
        return result;
    }
}
