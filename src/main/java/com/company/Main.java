/*
C.MURPHY
LAST UPDATE: 23/08/2018
 */

package com.company;

import java.io.File;

public class Main extends CSVReader {

    public static void main(String[] args) {

        // Re-used variables
        var db = new Database();
        var reader = new CSVReader();

        // reset the db
        db.dropTables(db.connect());

        // CSVReader setup
        reader.setSeparatorSymbol(",");
        reader.setCsvName(new File("src/main/resources/FL_insurance_sample.csv"));
        reader.setContentsOfFile(reader.readFile());
        reader.setFirstRowOfFile(reader.retrieveFirstRow());

        // Database setup
        db.createTableFromFirstRow(db.connect());

        // Policies setup
        var policies = reader.createPolicies(reader.getContentsOfFile());

        // Sql setup
        db.insertPolicies(policies, db.connect());
    }
}
