package com.company;

import java.sql.*;
import java.util.Date;
import java.util.List;

public class Database {

    /*
    Instance variables
     */

    private final String url = "jdbc:postgresql://localhost/fl-csvData";
    private final String user = "postgres";
    private final String password = "santiago";

    /*
    Class methods
     */

    // provides a connection via JDBC/Postgres driver
    public Connection connect() {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the PostgreSQL database successfully.");
        } catch (SQLException e) {
            System.out.println("Error code: " + e.getErrorCode());
        }

        return conn;
    }

    // creates table if necessary
    public void createTableFromFirstRow(Connection conn) {

        Statement stmnt = null;


        try {
            stmnt = conn.createStatement();

            // for checking query execution speed
            var startTime = new Date();


            System.out.println("Creating table in PostgreSQL...");
            String sql = ("CREATE TABLE insurance_samples " +
                    "(policyID INT NOT NULL PRIMARY KEY, " +
                    "statecode VARCHAR(50), " +
                    "county VARCHAR(50), " +
                    "eq_site_limit NUMERIC, " +
                    "hu_site_limit NUMERIC, " +
                    "fl_site_limit NUMERIC, " +
                    "fr_site_limit NUMERIC, " +
                    "tiv_2011 NUMERIC, " +
                    "tiv_2012 NUMERIC, " +
                    "eq_site_deductible NUMERIC, " +
                    "hu_site_deductible NUMERIC, " +
                    "fl_site_deductible NUMERIC, " +
                    "fr_site_deductible NUMERIC, " +
                    "point_latitude NUMERIC, " +
                    "point_longitude NUMERIC, " +
                    "line VARCHAR(50), " +
                    "construction VARCHAR(50), " +
                    "point_granularity INT " +
                    ")"
            );

            stmnt.execute(sql);
            System.out.println("Table successfully created.");

            var endTime = new Date();
            var timeResult = endTime.getTime() - startTime.getTime();
            System.out.println("Time to create table: " + timeResult + " ms");

        } catch (SQLException e) {
            System.out.println("Error creating table: " + e.getSQLState());
        } finally {
            try {
                if (stmnt != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println("Error: " + e.getErrorCode());
            }

        }

    }


    // uses batch processing to insert mapped Policy object information to Postgres
    public void insertPolicies(List<Policy> csvValues, Connection conn) {

        String sql = ("INSERT INTO insurance_samples(policyID,statecode,county,eq_site_limit,hu_site_limit," +
                "fl_site_limit,fr_site_limit,tiv_2011,tiv_2012,eq_site_deductible,hu_site_deductible," +
                "fl_site_deductible,fr_site_deductible,point_latitude,point_longitude," +
                "line,construction,point_granularity)" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");


        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            var startOfTotalTime = new Date();

            {

                int count = 0;
                var startOfDataProcessing = new Date();


                // batch queries is more efficient than individual rows
                for (Policy policy : csvValues) {
                    statement.setInt(1, policy.getPolicyID());
                    statement.setString(2, policy.getStateCode());
                    statement.setString(3, policy.getCounty());
                    statement.setFloat(4, policy.getEq_site_limit());
                    statement.setFloat(5, policy.getHu_site_limit());
                    statement.setFloat(6, policy.getFl_site_limit());
                    statement.setFloat(7, policy.getFr_site_limit());
                    statement.setFloat(8, policy.getTiv_2011());
                    statement.setFloat(9, policy.getTiv_2012());
                    statement.setFloat(10, policy.getEq_site_deductible());
                    statement.setFloat(11, policy.getHu_site_deductible());
                    statement.setFloat(12, policy.getFl_site_deductible());
                    statement.setFloat(13, policy.getFr_site_deductible());
                    statement.setFloat(14, policy.getPoint_latitude());
                    statement.setFloat(15, policy.getPoint_longitude());
                    statement.setString(16, policy.getLine());
                    statement.setString(17, policy.getConstruction());
                    statement.setInt(18, policy.getPoint_granularity());

                    statement.addBatch();
                    count++;

                    // execute patch for every 100 rows
                    if (count % 100 == 0 || count == csvValues.size()) {
                        statement.executeBatch();
                        var endOfBatch = new Date();
                        var timeResult = endOfBatch.getTime() - startOfDataProcessing.getTime();
                        startOfDataProcessing = new Date();
                        System.out.println("Time to execute batch no." + (count / 100) + ": " + timeResult + " ms");
                    }
                }

                var endOfTotalTime = new Date();
                var totalTime = endOfTotalTime.getTime() - startOfTotalTime.getTime();
                System.out.println("Total time for all queries: " + totalTime + " ms");
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getErrorCode());
            System.out.println("SQL code: " + e.getSQLState() + "\nPostgreSQL message: " + e.getMessage());

        }
    }

    // easy means of 'resetting' the program
    public void dropTables(Connection conn) {


        Statement stmnt = null;

        try {

            stmnt = conn.createStatement();
            String sql = "DROP TABLE insurance_samples";
            stmnt.execute(sql);

        } catch (SQLException e) {
            System.out.println("Error dropping table: " + e.getMessage());
        }
    }
}
