package com.company;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CSVReaderTest {

    @Test
    @DisplayName("should read csv file correctly and compare two results")
    public void checkReadFile() {

        /*
        Arrange
         */
        // setup variables
        var reader = new CSVReader();
        var firstRowOfCSV = new ArrayList();


        /*
        Act
         */
        // Read in file and store the first row
        reader.setCsvName(new File("src/main/resources/FL_insurance_sample.csv"));
        reader.setSeparatorSymbol(",");
        reader.setContentsOfFile(reader.readFile());

        // separate process to read file in case of compromised method
        try {
            System.out.println("Reading from CSV file.");
            Files.lines(reader.getCsvName().toPath(), StandardCharsets.UTF_8)
                    .map(line -> Arrays.asList(line.split(reader.getSeparatorSymbol())))
                    .findFirst()
                    .ifPresent(firstRowOfCSV::add);

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
        Assert
         */
        // Check if the values of the result and source match (first row)
        assertEquals(reader.getContentsOfFile().get(0), firstRowOfCSV.get(0));

    }

    @Test
    public void checkCreatePolicies() {

        /*
        Arrange
         */

        // CSV setup
        var reader = new CSVReader();
        reader.setSeparatorSymbol(",");
        reader.setCsvName(new File("src/main/resources/FL_insurance_sample.csv"));
        reader.setContentsOfFile(reader.readFile());
        reader.setFirstRowOfFile(reader.retrieveFirstRow());
        var csvFile = reader.getContentsOfFile();

        /*
        Act
         */

        // mimic the original method
        var result = new ArrayList<Policy>();
        String[] readableInfo = new String[csvFile.size()];
        var disassembledInfo = new ArrayList<String[]>();

        // first - split the object's contents into a readable array
        for (int i = 0; i < csvFile.size(); i++) {
            readableInfo[i] = csvFile.get(i).toString();
        }

        // second - split the array's entries into indie strings and store them too
        // array containing arrays containing a row's info
        for (int x = 1; x < csvFile.size(); x++) {
            disassembledInfo.add(readableInfo[x].split(","));
        }

        // third - map the values of the stored rows to a new Policy object
        for (String[] array : disassembledInfo) {

            for (int s = 0; s < array.length; s++) {

                // remove all commas, brackets, and whitespace
                array[s] = array[s].replaceAll("\\[", "");
                array[s] = array[s].replaceAll("\\]", "");
                array[s] = array[s].replaceAll("\\,", "");
                array[s] = array[s].trim();
            }


            Policy policy = new Policy(
                    Integer.parseInt(array[0]), array[1], array[2], Float.parseFloat(array[3]),
                    Float.parseFloat(array[4]), Float.parseFloat(array[5]), Float.parseFloat(array[6]),
                    Float.parseFloat(array[7]), Float.parseFloat(array[8]), Float.parseFloat(array[9]),
                    Float.parseFloat(array[10]), Float.parseFloat(array[11]), Float.parseFloat(array[12]),
                    Float.parseFloat(array[13]), Float.parseFloat(array[14]), array[15], array[16], Integer.parseInt(array[17])
            );

            // collect the new policies
            result.add(policy);


        }

        /*
        Assert
         */

        // checks the policy collection's values
        boolean policiesMoreThanOne = result.size() > 1;
        boolean policiesArePolicies = result.get(1) instanceof Policy;
        boolean firstRowAndPolicyAreEqual = csvFile.get(1).toString().contains(result.get(1).getCounty());
        assertTrue(policiesArePolicies);
        assertTrue(policiesMoreThanOne);
        assertTrue(firstRowAndPolicyAreEqual);


    }

}
