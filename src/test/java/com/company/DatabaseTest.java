package com.company;

import org.junit.jupiter.api.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

class DatabaseTest {

    // for cross-database compatibility
    // run the test after executing main
    @Test
    void checkForTable() {

        /*
        Arrange
         */

        var db = new Database();
        var conn = db.connect();
        var check = false;

        /*
        Act
         */

        try {

            // metadata allows easy check
            var metaData = conn.getMetaData();
            var resultSet = metaData.getTables(null, null, "insurance_samples", null);

            while (resultSet.next()) {
                if (resultSet.getString(3).equals("insurance_samples")) {
                    check = true;
                    System.out.println("Table exists: " + resultSet.getString(3));
                }
            }

        } catch (SQLException e) {
            System.out.println("Error checking table: " + e.getMessage());
        }

        /*
        Assert
         */

        assertTrue(check);
    }

    // run the test after executing main
    @Test
    void checkInsertPolicies() {

        /*
        Arrange
         */

        var db = new Database();
        var conn = db.connect();
        PreparedStatement stmnt;


        /*
        Act
         */

        try {
            String sql = ("SELECT COUNT(2) FROM insurance_samples");
            stmnt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);


            ResultSet results = (stmnt).executeQuery();
            boolean containsRows = results.first();

        /*
        Assert
         */

            assertTrue(containsRows);

        } catch (SQLException e) {
            System.out.println("Error checking rows: " + e.getMessage());
        }

    }

    @Test
        // for checking whether the db actually resets
    void checkDropTables() {

        
        /*
        Arrange
         */

        var db = new Database();
        var conn = db.connect();
        var check = false;
        db.dropTables(db.connect());

        /*
        Act
         */

        try {

            // metadata allows easy check
            var metaData = conn.getMetaData();
            var resultSet = metaData.getTables(null, null, "insurance_samples", null);

            while (resultSet.next()) {
                if (resultSet.getString(3).equals("insurance_samples")) {
                    check = true;
                }
            }

        } catch (SQLException e) {
            System.out.println("Error checking table: " + e.getMessage());
        }

        /*
        Assert
         */

        assertFalse(check);
    }
}