// LAST UPDATE - 24/08/2018

Project details:

Language(s): Java  <br>
IDE: IntelliJ Community 2018 <br>
Test framework: JUnit 5 <br>
Dependency manager: Maven <br>
Resources: .csv, PostgreSQL driver <br>
Database(s): PostgreSQL <br>

Instructions: <br>

Can be run as project from IntelliJ, results will be displayed in the console
including: <br> query execution time in ms, sqlexceptions, status of database